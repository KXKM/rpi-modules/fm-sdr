#!/bin/bash
BASEPATH="$(dirname "$(readlink -f "$0")")"

pacman -S rtl-sdr --noconfirm --needed
pip install -r "$BASEPATH/requirements.txt"

# cd "$BASEPATH/rtl_fm_python/"
# ./build.sh

mkdir -p "/data/var/fm-sdr"

ln -sf "$BASEPATH/fm-sdr.service" /etc/systemd/system/
ln -sf "$BASEPATH/fm-sdr" /usr/local/bin/
ln -sf "$BASEPATH/fm-sdr/www" /data/var/fm-sdr/www

systemctl daemon-reload

FILE=/boot/starter.txt
if test -f "$FILE"; then
echo "## [fm-sdr] Play FM radio with USB-SDR dongle
# fm-sdr
" >> /boot/starter.txt
fi