#!/usr/bin/python3
# rtl-sdr, turns your Realtek RTL2832 based DVB dongle into a SDR receiver
# Copyright (C) 2012 by Steve Markgraf <steve@steve-m.de>
# Copyright (C) 2012 by Hoernchen <la@tfc-server.de>
# Copyright (C) 2012 by Kyle Keen <keenerd@gmail.com>
# Copyright (C) 2013 by Elias Oenal <EliasOenal@gmail.com>
# Copyright (C) 2014 by Thomas Winningham <winningham@gmail.com>
# Copyright (C) 2020 by Thomas Bohl <thomas.bohl@37m.gr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, jsonify, url_for, redirect
from rtl_fm_python_thread import *
import json, sys

# 
# SETTINGS LOAD
# 

settings_path = '/data/var/fm-sdr/settings.json'

settings = {
    'frequency': int(103.0*1e6),
    'volume': 50,
    'amp': True
}

try:
    with open(settings_path) as json_file:
        settings = json.load(json_file)
    print("Settings loaded from", settings_path)
except:
    with open(settings_path, 'w') as json_file:
        json.dump(settings, json_file)
    print("Settings file is missing, writing default one.")


def settings_save():
    with open(settings_path, 'w') as json_file:
        json.dump(settings, json_file)

#
# ALSA VOLUME
#
def set_volume(v):
    pass


#
# AMPLIFIER ON/OFF
#
def get_amp():
    return settings['amp']

def set_amp(onoff):
	settings['amp'] = onoff

# 
# INIT RTL_FM
# 

args = sys.argv[1:] + ['-f', str(settings['frequency'])]

set_volume(settings['volume'])
make_rtl_fm_thread(args=args, block=False)

# 
# START WEB INTERFACE
# 

app = Flask(__name__)

@app.route('/')
def web_root():
	return redirect(url_for('static', filename='index.html'))

@app.route('/state')
def web_state():
	return jsonify(
		{
			's_level'	: get_s_level(),
			'freq_s' 	: get_freq_human(),
			'freq_i' 	: get_frequency(),
			'amp' 	    : get_amp(),
			# 'mod'		: get_demod(),
			# 'gain'		: get_gain(),
			# 'autogain'	: get_auto_gain()
		})

@app.route('/frequency/<int:f>')
def web_set_frequency(f):
	set_frequency(f)
	settings['frequency'] = str(f)
	settings_save()
	return web_state()

@app.route('/frequency/mhz/<float:f>')
@app.route('/frequency/mhz/<int:f>')
def web_set_mhz_frequency(f):
	freq = int(f*1e6)
	set_frequency( freq )
	settings['frequency'] = freq
	settings_save()
	return web_state()

@app.route('/volume/<int:v>')
def web_set_volume(v):
	set_volume(v)
	settings['volume'] = v
	settings_save()
	return web_state()

@app.route('/amp/<a>')
def web_set_amp(a):
	set_amp(a=='on')
	settings['volume'] = (a=='on')
	settings_save()
	return web_state()

# @app.route('/demod/<c>')
# def web_set_demod(c):
# 	set_demod(str(c))
# 	return web_state()

# @app.route('/gain/<g>')
# def web_set_gain(g):
# 	gain = int(str(g))
# 	set_gain(gain)
# 	return web_state()

# @app.route('/gain/human/<g>')
# def web_set_gain_human(g):
# 	gain = int(str(g))
# 	set_gain_human(gain)
# 	settings['gain'] = gain
# 	settings_save()
# 	return web_state()

# @app.route('/gain/auto')
# def web_set_auto_gain():
# 	set_auto_gain()
# 	return web_state()

# @app.route('/gain/list')
# def web_get_gain_list():
# 	l=get_gains()
# 	return jsonify({'gains':l})

if __name__ == '__main__':
	app.run(host='0.0.0.0',port=9999)
	stop_thread()

